//
//  ViewController.swift
//  UserDefaults
//
//  Created by Юра on 26.06.2024.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var changeColorButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let savedColorData = UserDefaults.standard.data(forKey: "backgroundColor"),
                   let savedColor = try? NSKeyedUnarchiver.unarchivedObject(ofClass: UIColor.self, from: savedColorData) {
                    self.view.backgroundColor = savedColor
                } else {
                    self.view.backgroundColor = .white
                }
    
    }

    @IBAction func changeColorButtonTapped(_ sender: UIButton) {
        let randomColor = generateRandomColor()
                self.view.backgroundColor = randomColor

                // Сохранение цвета фона в UserDefaults
                if let colorData = try? NSKeyedArchiver.archivedData(withRootObject: randomColor, requiringSecureCoding: false) {
                    UserDefaults.standard.set(colorData, forKey: "backgroundColor")
                }
        
    }
    
    func generateRandomColor() -> UIColor {
            let red = CGFloat.random(in: 0...1)
            let green = CGFloat.random(in: 0...1)
            let blue = CGFloat.random(in: 0...1)
            return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
        }
    
}

