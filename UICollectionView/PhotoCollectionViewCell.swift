//
//  PhotoCollectionViewCell.swift
//  CollectionView
//
//  Created by Юра on 25.06.2024.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
