struct Car {
    let brand: String 
    let year: Int 
    var trunkVolume: Int 
    var engineRunning: Bool 
    var windowsOpen: Bool 
    var loadedTrunkVolume: Int
    mutating func performAction(_ action: CarAction) {
            switch action {
            case .startEngine:
                engineRunning = true
            case .stopEngine:
                engineRunning = false
            case .openWindows:
                windowsOpen = true
            case .closeWindows:
                windowsOpen = false
            case .loadTrunk(let volume):
                loadedTrunkVolume = min(loadedTrunkVolume + volume, trunkVolume)
            case .unloadTrunk(let volume):
                loadedTrunkVolume = max(loadedTrunkVolume - volume, 0)
            }
        }
}

struct Truck {
    let brand: String 
    let year: Int 
    var bodyVolume: Int 
    var engineRunning: Bool 
    var windowsOpen: Bool 
    var loadedBodyVolume: Int 
    mutating func performAction(_ action: CarAction) {
            switch action {
            case .startEngine:
                engineRunning = true
            case .stopEngine:
                engineRunning = false
            case .openWindows:
                windowsOpen = true
            case .closeWindows:
                windowsOpen = false
            case .loadTrunk(let volume):
                loadedBodyVolume = min(loadedBodyVolume + volume, bodyVolume)
            case .unloadTrunk(let volume):
                loadedBodyVolume = max(loadedBodyVolume - volume, 0)
            }
        }
}

enum CarAction {
    case startEngine
    case stopEngine 
    case openWindows
    case closeWindows
    case loadTrunk(volume: Int)
    case unloadTrunk(volume: Int)
}

var car1 = Car(brand: "Ford", year: 2020, trunkVolume: 500, engineRunning: false, windowsOpen: false, loadedTrunkVolume: 0)
var truck1 = Truck(brand: "MAN", year: 2015, bodyVolume: 20, engineRunning: false, windowsOpen: true, loadedBodyVolume: 15)

car1.performAction(.startEngine)
car1.performAction(.loadTrunk(volume: 300))

var dict = [String: Any]()

dict["car1"] = car1
dict["truck1"] = truck1


class Player {
    func playGame(){
        print("Playing CS 2")
    }
}

func play() -> () -> Void {
    let Denis = Player()
    
    let playing = { [weak Denis] in // слабый захват
        Denis?.playGame()
        return
    }
    
    return playing
}
/*
class Car1 {
    var driver: Man?
    deinit{
        print("Машина удалена из памяти")
    }
}

class Man {
    weak var myCar: Car1?
    deinit{
        print("Мужчина удален из памяти")
    }
}

var car: Car1? = Car1()
var man: Man? = Man()

car?.driver = man 
man?.myCar = car

car = nil
man = nil
*/

class Man {
    var passport: Passport?
    deinit{
        print("Мужчина удален из памяти")
    }
}

class Passport {
    unowned let man: Man
    init(man: Man){
        self.man = man
    }
    deinit{
        print("Паспорт удален из памяти")
    }
}

var man: Man? = Man()
var passport: Passport? = Passport(man: man!)
man?.passport = passport

passport = nil
man = nil
