//
//  FavoritesViewController.swift
//  NYTArticles
//
//  Created by Юра on 29.06.2024.
//

import UIKit

class FavoritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var favoriteArticles: [Article] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Избранное"
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "FavoriteArticleCell")
        
        loadFavoriteArticles()
        
        NotificationCenter.default.addObserver(self, selector: #selector(favoritesUpdated), name: NSNotification.Name("FavoritesUpdated"), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            loadFavoriteArticles()
        }
    
    @objc func favoritesUpdated(notification: NSNotification) {
            if let newFavoriteArticle = notification.object as? Article {
                favoriteArticles.append(newFavoriteArticle)
                tableView.reloadData()
            }
        }
    
    func loadFavoriteArticles() {
        if let savedArticles = UserDefaults.standard.object(forKey: "favoriteArticles") as? Data {
            let decoder = JSONDecoder()
            if let articles = try? decoder.decode([Article].self, from: savedArticles) {
                favoriteArticles = articles
            }
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteArticles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteArticleCell", for: indexPath)
        let article = favoriteArticles[indexPath.row]
        cell.textLabel?.text = article.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let article = favoriteArticles[indexPath.row]
        performSegue(withIdentifier: "showDetailSegue", sender: article)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "showDetailSegue" {
                if let destinationVC = segue.destination as? ArticleDetailViewController {
                    destinationVC.article = sender as? Article
                }
            }
        }
}


