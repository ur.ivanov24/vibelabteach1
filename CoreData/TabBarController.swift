//
//  TabBarController.swift
//  NYTArticles
//
//  Created by Юра on 29.06.2024.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let articleListVC = ArticleListViewController()
        articleListVC.tabBarItem = UITabBarItem(tabBarSystemItem: .mostViewed, tag: 0)

        let favoritesVC = FavoritesViewController()
        favoritesVC.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 1)

        viewControllers = [UINavigationController(rootViewController: articleListVC),
                           UINavigationController(rootViewController: favoritesVC)]
    }
}

