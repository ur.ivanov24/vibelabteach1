//
//  ArticleListViewController.swift
//  NYTArticles
//
//  Created by Юра on 28.06.2024.
//

import UIKit

class ArticleListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var articles: [Article] = []
    let apiService = APIService()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Статьи"
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "ArticleCell")
        
        loadArticles()
        
        NotificationCenter.default.addObserver(self, selector: #selector(favoritesUpdated), name: NSNotification.Name("FavoritesUpdated"), object: nil)
    }
    
    func loadArticles() {
        apiService.fetchArticles { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let articles):
                    self?.articles = articles
                    self?.tableView.reloadData()
                case .failure(let error):
                    // Обработка ошибки (например, показать сообщение пользователю)
                    print("Failed to fetch articles: \(error)")
                }
            }
        }
    }
    
    @objc func favoritesUpdated(notification: NSNotification) {
        if let favoriteArticle = notification.object as? Article {
                    if let index = articles.firstIndex(where: { $0.title == favoriteArticle.title }) {
                        articles.remove(at: index)
                        tableView.reloadData()
                    }
                }
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath)
        let article = articles[indexPath.row]
        cell.textLabel?.text = article.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let article = articles[indexPath.row]
        performSegue(withIdentifier: "showDetailSegue", sender: article)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "showDetailSegue" {
                if let destinationVC = segue.destination as? ArticleDetailViewController {
                    destinationVC.article = sender as? Article
                }
            }
        }
}


