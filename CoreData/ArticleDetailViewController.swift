//
//  ArticleDetailViewController.swift
//  NYTArticles
//
//  Created by Юра on 28.06.2024.
//

import UIKit

class ArticleDetailViewController: UIViewController {

    var article: Article?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = article?.title
        view.backgroundColor = .white

        if let article = article {
            let titleLabel = UILabel()
            titleLabel.text = article.title
            titleLabel.frame = CGRect(x: 20, y: 100, width: view.frame.width - 40, height: 30)
            view.addSubview(titleLabel)

            let abstractLabel = UILabel()
            abstractLabel.text = article.abstract
            abstractLabel.frame = CGRect(x: 20, y: 150, width: view.frame.width - 40, height: 100)
            view.addSubview(abstractLabel)

            let favoriteButton = UIButton(type: .system)
            favoriteButton.setTitle("Добавить в избранное", for: .normal)
            favoriteButton.addTarget(self, action: #selector(addToFavorites), for: .touchUpInside)
            favoriteButton.frame = CGRect(x: 20, y: 300, width: 200, height: 50)
            view.addSubview(favoriteButton)
        }
    }

    @objc func addToFavorites() {
        guard let article = article else { return }

        var favoriteArticles: [Article] = []
        if let savedArticles = UserDefaults.standard.object(forKey: "favoriteArticles") as? Data {
            let decoder = JSONDecoder()
            if let articles = try? decoder.decode([Article].self, from: savedArticles) {
                favoriteArticles = articles
            }
        }
        favoriteArticles.append(article)

        // Проверяем, есть ли уже такая статья в избранном, чтобы избежать дублирования
        if !favoriteArticles.contains(where: { $0.title == article.title }) {
                favoriteArticles.append(article)
        }
        
        
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(favoriteArticles) {
            UserDefaults.standard.set(encoded, forKey: "favoriteArticles")
        }
        
        // Notify about favorite update
        NotificationCenter.default.post(name: NSNotification.Name("FavoritesUpdated"), object: article)
        
        
        navigationController?.popViewController(animated: true)
    }
}



