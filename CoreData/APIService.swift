//
//  APIService.swift
//  NYTArticles
//
//  Created by Юра on 28.06.2024.
//

import Foundation

class APIService { // Этот код загружает статьи при загрузке ArticleListViewController и обновляет таблицу с полученными данными.
    static let shared = APIService()
    
    func fetchArticles(completion: @escaping (Result<[Article], Error>) -> Void) {         // Этот метод выполняет сетевой запрос для получения списка статей. Он принимает замыкание completion, которое вызывается после завершения запроса с результатом.
        let urlString = "https://api.nytimes.com/svc/mostpopular/v2/viewed/30.json?api-key=\(Config.apiKey)"
        
        guard let url = URL(string: urlString) else {
            completion(.failure(NSError(domain: "Invalid URL", code: -1, userInfo: nil)))
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data else {
                completion(.failure(NSError(domain: "No data", code: -1, userInfo: nil)))
                return
            }
            
            do {
                let articleResponse = try JSONDecoder().decode(ArticleResponse.self, from: data)
                completion(.success(articleResponse.results))
            } catch {
                completion(.failure(error))
            }
        }.resume()
    }
}

