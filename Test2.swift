let daysInMonths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

let monthNames = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]

print("Количество дней в каждом месяце:")
for days in daysInMonths {
	print(days)
}

print("\nНазвание месяца и количество дней:")
for (index, month) in monthNames.enumerated() {
	print("\(month): \(daysInMonths[index])")
}

var monthInfo: [(String, Int)] = []
for i in 0..<monthNames.count {
	monthInfo.append((monthNames[i], daysInMonths[i]))
}

print("\nНазвание месяца и количество дней:")
for info in monthInfo {
	print("\(info.0): \(info.1)")
}

print("\nДни в обратном порядке:")
for days in daysInMonths.reversed() {
	print(days)
}

func daysUntilDate(month: Int, day: Int) -> Int {
	var totalDays = 0
	for i in 0..<month - 1 {
		totalDays += daysInMonths[i]
	}
	totalDays += day
	return totalDays
}

let daysUntilApril15 = daysUntilDate(month: 4, day: 15)
print("\nДней до 15 апреля: \(daysUntilApril15)")