// Сортировка массива с помощью замыкания в одну и обратную сторону
func sortArray(_ arr: inout [String], by closure: (String, String) -> Bool) {
    arr.sort(by: closure)
}

// Создание метода для сортировки имен друзей по количеству букв
func sortFriendsByNameLength(_ names: inout [String]) {
    names.sort { $0.count < $1.count }
}

// Создание словаря, где ключ - количество символов в имени, а значение - имя друга
func createDictionary(_ names: [String]) -> [Int: String] {
    var dictionary = [Int: String]()
    for name in names {
        dictionary[name.count] = name
    }
    return dictionary
}

// Вывод ключа и значения словаря
func printDictionaryKeyValue(key: Int, value: String) {
    print("Key: \(key), Value: \(value)")
}

// Проверка и вывод массива на пустоту
func checkAndPrintArrays(array1: [String], array2: [Int]) {
    if array1.isEmpty {
        print("Первый массив пуст, добавляем элемент")
        // Добавляем любое значение в пустой массив
        var newArray1 = array1
        newArray1.append("Пример")
        print("Массив 1: \(newArray1)")
    } else {
        print("Массив 1: \(array1)")
    }
    
    if array2.isEmpty {
        print("Второй массив пуст, добавляем элемент")
        // Добавляем любое значение в пустой массив
        var newArray2 = array2
        newArray2.append(123)
        print("Массив 2: \(newArray2)")
    } else {
        print("Массив 2: \(array2)")
    }
}

