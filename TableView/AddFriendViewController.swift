//
//  AddFriendViewController.swift
//  TableView
//
//  Created by Юра on 24.06.2024.
//

import UIKit

protocol AddUserDelegate: AnyObject {
    func didAddUser(_ user: UserModel)
}

class AddFriendViewController: UIViewController {

    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var addName: UITextField!
    @IBOutlet weak var addAge: UITextField!
    @IBOutlet weak var addLastName: UITextField!
    @IBOutlet weak var addEmail: UITextField!
    @IBOutlet weak var addPhone: UITextField!
    @IBOutlet weak var addShool: UITextField!
    @IBOutlet weak var addGender: UITextField!
    
    weak var delegate: AddUserDelegate?
        
    @IBAction func savebuttonTapped(_ sender: UIButton) {
        guard let name = addName.text, !name.isEmpty,
              let age = addAge.text, !age.isEmpty,
              let gender = addGender.text, !gender.isEmpty,
              let phone = addPhone.text, isValidPhone(phone) else {
                showAlert()
                return
            }
        
        let lastName = addLastName.text
        let schoolOrWork = addShool.text
        let email = addEmail.text
        
        let user = UserModel(name: name, age: age, lastName: lastName, Gender: gender, schoolOrWork: schoolOrWork, phone: phone, email: email)
        delegate?.didAddUser(user)
        navigationController?.popViewController(animated: true)
        
    }
   
        
        func isValidPhone(_ phone: String) -> Bool {
            let phoneRegex = "^[0-9]{10}$"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return phoneTest.evaluate(with: phone)
        }
        
        func showAlert() {
            let alert = UIAlertController(title: "Error", message: "Заполните поля корректно.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            present(alert, animated: true)
        }
    }

    


