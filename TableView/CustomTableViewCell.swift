//
//  CustomTableViewCell.swift
//  TableView
//
//  Created by Юра on 24.06.2024.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    
    @IBOutlet weak var LabelOne: UILabel!
    
    @IBOutlet weak var LabelTwo: UILabel!
    
    func configure(with user:UserModel){
        LabelOne.text = user.name
        LabelTwo.text = user.age
        
    }
    
}
