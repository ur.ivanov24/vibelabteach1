//
//  DetailsViewController.swift
//  TableView
//
//  Created by Юра on 24.06.2024.
//

import UIKit

class DetailsViewController: UIViewController {

    
    @IBOutlet weak var nameLabel: UILabel!
    
    
    @IBOutlet weak var ageLabel: UILabel!
    
    @IBOutlet weak var lastNameLabel: UILabel!
    
    
    @IBOutlet weak var genderLabel: UILabel!
    
    
    @IBOutlet weak var schoolOrWorkLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    
    
    @IBOutlet weak var emailLabel: UILabel!
    
    var user: UserModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = user.name
        ageLabel.text = user.age
        lastNameLabel.text = user.lastName
        genderLabel.text = user.Gender
        schoolOrWorkLabel.text = user.schoolOrWork
        phoneLabel.text = user.phone
        emailLabel.text = user.email
        
        phoneLabel.sizeToFit()
        
    }
    

    

}
