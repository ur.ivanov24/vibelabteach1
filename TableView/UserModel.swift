//
//  UserModel.swift
//  TableView
//
//  Created by Юра on 24.06.2024.
//

import Foundation

struct UserModel {
    var name: String
    var age: String
    var lastName: String?
    var Gender: String
    var schoolOrWork: String?
    var phone: String
    var email: String?
}
