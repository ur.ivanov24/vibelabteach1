//
//  ViewController.swift
//  TableView
//
//  Created by Юра on 24.06.2024.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, AddUserDelegate {

    
    @IBOutlet weak var Button: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    var users: [UserModel] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        users.append(UserModel(name: "Yuri", age: "18", Gender: "Male", phone: "79117893208"))
       
        
        tableView.delegate = self
        tableView.dataSource = self
    
    }
    
    @IBAction func PressedAdd(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let addUserVC = storyboard.instantiateViewController(withIdentifier: "AddFriendViewController") as? AddFriendViewController {
            addUserVC.delegate = self
            navigationController?.pushViewController(addUserVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! CustomTableViewCell
        let model = users[indexPath.row]
        cell.configure(with: model)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = users[indexPath.row]
        performSegue(withIdentifier: "detailIdentifier", sender: model)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailIdentifier", let userModel = sender as? UserModel{
            let destController = segue.destination as! DetailsViewController
            destController.user = userModel
        }
    }
    
    func didAddUser(_ user: UserModel) {
            users.append(user)
            tableView.reloadData()
        }

}

