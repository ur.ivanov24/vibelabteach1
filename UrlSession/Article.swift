//
//  Article.swift
//  NYTArticles
//
//  Created by Юра on 28.06.2024.
//

import Foundation

struct ArticleResponse: Codable {
    let results: [Article]
}

struct Article: Codable {
    let title: String
    let abstract: String
    let url: String
    let media: [Media]
}

struct Media: Codable {
    let mediaMetadata: [MediaMetadata]
    
    enum CodingKeys: String, CodingKey {
        case mediaMetadata = "media-metadata"
    }
}

struct MediaMetadata: Codable {
    let url: String
}

