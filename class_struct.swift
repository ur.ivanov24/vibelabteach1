class Weapon{
	var description : String = "Оружие"
}

class Pistol : Weapon {
	var bulletspeed : Int = 10
}

class Shotgun : Weapon {
	var bulletspeed : Int = 5
}

class House {
	var width: Int
	var height: Int
	
	init(width: Int, height: Int) {
		self.width = width
		self.height = height
	}
	
	func createHouse() {
		print("Дом построен. Площадь: \(width * height)")
	}
	
	func destroyHouse() {
		print("Дом разрушен")
	}
}

class Student {
	var name: String
	var age: Int
	var averageScore: Double
	
	init(name: String, age: Int, averageScore: Double) {
		self.name = name
		self.age = age
		self.averageScore = averageScore
	}
	func sortByAge(students: [Student]) -> [Student] {
			return students.sorted { $0.age < $1.age }
		}
	func sortByScore(students: [Student]) -> [Student] {
			return students.sorted { $0.averageScore > $1.averageScore }
		}
}


struct StudentStruct {
	var name: String
	var age: Int
}

class StudentClass {
	var name: String
	var age: Int
	
	init(name: String, age: Int) {
		self.name = name
		self.age = age
	}

}

// Структуры копируются при передаче или присваивании, а классы передаются по ссылке.
// Структуры не поддерживают наследование, а классы поддерживают
// Классы предпочтительны, когда нужно создавать более сложные объекты.
// Структуры предпочтительны для хранения небольших данных.

import Foundation


struct Card {
	let suit: String // Черви, Бубны, Трефы, Пики
	let rank: String // 2, 3, ..., 10, J, Q, K, A
}

func randomCard() -> Card {
	let suits = ["Черви", "Бубны", "Трефы", "Пики"]
	let ranks = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]
	
	let randomSuitIndex = Int.random(in: 0..<suits.count)
	let randomRankIndex = Int.random(in: 0..<ranks.count)
	
	return Card(suit: suits[randomSuitIndex], rank: ranks[randomRankIndex])
}

func isOnePair(cards: [Card]) -> Bool { // проверка на пару
	var ranksCount = [String: Int]()
	for card in cards {
		ranksCount[card.rank, default: 0] += 1
	}
	
	return ranksCount.values.contains(2)
}

func isTwoPairs(cards: [Card]) -> Bool { // проверка на две пары
	var ranksCount = [String: Int]()
	for card in cards {
		ranksCount[card.rank, default: 0] += 1
	}
	
	var pairsCount = 0
	for count in ranksCount.values {
		if count == 2 {
			pairsCount += 1
		}
	}
	
	return pairsCount == 2
}

func isSet(cards: [Card]) -> Bool { // проверка на сет
	var ranksCount = [String: Int]()
	for card in cards {
		ranksCount[card.rank, default: 0] += 1
	}
	
	return ranksCount.values.contains(3)
}
func isStraight(cards: [Card]) -> Bool { // проверка на стрит 
	let sortedCards = cards.sorted { $0.rank < $1.rank }
	
	for i in 1..<sortedCards.count {
		if sortedCards[i].rank != String(Int(sortedCards[i - 1].rank)! + 1) {
			return false
		}
	}
	
	return true
}

func isFlush(cards: [Card]) -> Bool {
	var suitsCount = [String: Int]()
	for card in cards {
		suitsCount[card.suit, default: 0] += 1
	}
	
	return suitsCount.values.contains(5) // 5 карт одной масти
}

func checkCombination(cards: [Card]) {
	if isTwoPairs(cards: cards) {
		print("У вас две пары!")
	} else if isSet(cards: cards) {
		print("У вас сет!")
	} else if isStraight(cards: cards) {
		print("У вас стрит!")
	} else if isOnePair(cards: cards) {
		print("У вас пара!")
	} else if isFlush(cards: cards){
		print("У вас флеш!")
	} else {print("У вас нет комбинации!")}
	
}



let cards = [randomCard(), randomCard(), randomCard(), randomCard(), randomCard()]
print(cards)
checkCombination(cards: cards)