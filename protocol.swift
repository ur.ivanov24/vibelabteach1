struct Identifier {
	var id = 1
}

class Ref<T> {
	var value : T
	init(value:T){
		self.value = value
	}
}

struct IOSCollection<T>{
	var ref : Ref<T>
	init(value:T){
		self.ref = Ref(value: value)
	}
	
	var value : T {
		get{
			ref.value
		}
		set{
			guard isKnownUniquelyReferenced(&ref) else {
				ref = Ref(value: newValue)
				return
			}
			ref.value = newValue
		}
	}
}

var id = Identifier()
var collection1 = IOSCollection(value: id)
var collection2 = collection1

func address<T>(off object: T) {
	print(Unmanaged.passUnretained(object as AnyObject).toOpaque())
}

address(off: collection1.ref)
address(off: collection2.ref)

collection2.value.id = 2

address(off: collection1.ref)
address(off: collection2.ref)



protocol Hotel {
	init(roomCount: Int)
}

class HotelAlfa: Hotel {
	var roomCount: Int
	
	required init(roomCount: Int) {
		self.roomCount = roomCount
	}
}

protocol GameDice {
	var numberDice: Int { get }
}

extension Int: GameDice {
	var numberDice: Int {
		return self
	}
}


protocol MyProtocol {
	var name: String { get }
	var year: Int? { get set }
	
	func printData()
}

class Class: MyProtocol {
	var name: String
		
	var year: Int?
	
	init(name: String, year: Int? = nil) {
		self.name = name
		self.year = year
	}
	
	func printData() {
		print("Имя: \(name)")
		if let year = year {
			print("Год: \(year)")
		}
	}
}

enum Platform {
	case ios
	case android
	case web
}
protocol CodingProject {
	var time: Int { get }
	var linesOfCode: Int { get }
	
	func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol CodingStop {
	func stopCoding()
}

class Company: CodingProject, CodingStop {
	var numberOfProgrammers: Int
	var specializations: [Platform]
	
	init(numberOfProgrammers: Int, specializations: [Platform]) {
		self.numberOfProgrammers = numberOfProgrammers
		self.specializations = specializations
	}
	
	var time: Int {
		var totalTime = 0
		for platform in specializations {
			switch platform {
			case .ios:
				totalTime += 40
			case .android:
				totalTime += 20
			case .web:
				totalTime += 50
			}
		}
		return totalTime * numberOfProgrammers
	}
	
	var linesOfCode: Int {
		var totalLines = 0
		for platform in specializations {
			switch platform {
			case .ios:
				totalLines += 200
			case .android:
				totalLines += 300
			case .web:
				totalLines += 500
			}
		}
		return totalLines * numberOfProgrammers
	}
	
	func writeCode(platform: Platform, numberOfSpecialist: Int) {
		print("Разработка на платформе \(platform) началась. Пишем код...")
		print("Работа на платформе \(platform) закончена. Сдаю в тестирование.")
	}
	
	func stopCoding() {
		print("Разработка остановлена.")
	}
}

let company = Company(numberOfProgrammers: 10, specializations: [.ios, .android, .web])


company.writeCode(platform: .ios, numberOfSpecialist: 5)