enum Weekday : Int {
	case Monday
	case Tuesday
	case Wednesday
	case Thursday
	case Friday
	case Saturday
	case Sunday
}

enum Fruits: String {
	case apple = "Apple"
	case banana = "Banana"
	case orange = "Orange"
}

enum Gender {
	case male
	case female
}

enum AgeCategory {
	case adult
	case old
}

enum Experience {
	case junior
	case middle
	case senior
}

enum RainbowColor {
	case red
	case orange
	case yellow
	case green
	case blue
	case indigo
	case violet
}

func printRainbowColors() {
	let rainbowColors: [RainbowColor] = [.red, .orange, .yellow, .green, .blue, .indigo, .violet]
	for color in rainbowColors {
		switch color {
		case .red:
			print("apple red")
		case .orange:
			print("orange orange")
		case .yellow:
			print("lemon yellow")
		case .green:
			print("grass green")
		case .blue:
			print("sky blue")
		case .indigo:
			print("deep blue")
		case .violet:
			print("violet violet")
		}
	}
}

enum Score: String {
	case excellent = "A"
	case good = "B"
	case satisfactory = "C"
	case needsImprovement = "D"
	case fail = "F"
}
func numericalValue(for score: Score) -> Int {
	switch score {
	case .excellent:
		return 5
	case .good:
		return 4
	case .satisfactory:
		return 3
	case .needsImprovement:
		return 2
	case .fail:
		return 1
	}
}

enum Cars: String {
	case mercedes = "Мерседес"
	case bmw = "БМВ"
	case lada = "Лада"
}

func printCars(cars: [Cars]) {
	print("В гараже:")
	for car in cars {
		print("\(car.rawValue)")
	}
}

let garageCars: [Cars] = [.mercedes, .bmw, .lada]
printCars(cars: garageCars)